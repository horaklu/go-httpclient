package gohttp

import (
	"bitbucket.org/horaklu/go-httpclient/core"
	"net/http"
	"sync"
)

type httpClient struct {
	builder    *clientBuilder
	client     *http.Client
	clientOnce sync.Once
}

type Client interface {
	Get(url string, headers ...http.Header) (*core.Response, error)
	Post(url string, body interface{}, headers ...http.Header) (*core.Response, error)
	Put(url string, body interface{}, headers ...http.Header) (*core.Response, error)
	Patch(url string, body interface{}, headers ...http.Header) (*core.Response, error)
	Delete(url string, headers ...http.Header) (*core.Response, error)
	Options(url string, headers ...http.Header) (*core.Response, error)
}

func (hc *httpClient) Get(url string, headers ...http.Header) (*core.Response, error) {
	return hc.do(http.MethodGet, url, getHeaders(headers...), nil)
}
func (hc *httpClient) Post(url string, body interface{}, headers ...http.Header) (*core.Response, error) {
	return hc.do(http.MethodPost, url, getHeaders(headers...), body)
}
func (hc *httpClient) Put(url string, body interface{}, headers ...http.Header) (*core.Response, error) {
	return hc.do(http.MethodPut, url, getHeaders(headers...), body)
}
func (hc *httpClient) Patch(url string, body interface{}, headers ...http.Header) (*core.Response, error) {
	return hc.do(http.MethodPatch, url, getHeaders(headers...), body)
}
func (hc *httpClient) Delete(url string, headers ...http.Header) (*core.Response, error) {
	return hc.do(http.MethodDelete, url, getHeaders(headers...), nil)
}
func (hc *httpClient) Options(url string, headers ...http.Header) (*core.Response, error) {
	return hc.do(http.MethodOptions, url, getHeaders(headers...), nil)
}
