package gohttp

import (
	"net/http"
	"testing"
)

func Test_httpClient_getRequestBody(t *testing.T) {
	client := httpClient{}

	t.Run("noBodyNilResponse", func(t *testing.T) {
		body, err := client.getRequestBody("", nil)
		if err != nil {
			t.Error("no error expected when passing a nil body")
		}
		if body != nil {
			t.Error("no body expected when passing a nil body")
		}
	})

	t.Run("BodyWithJSON", func(t *testing.T) {

		requestBody := []string{"one", "two"}
		body, err := client.getRequestBody("application/json", requestBody)

		if err != nil {
			t.Error("no error expected when marshalling slice as JSON")
		}
		if string(body) != `["one","two"]` {
			t.Error("invalid JSON body obtained")
		}
	})

	t.Run("BodyWithXML", func(t *testing.T) {

		requestBody := []string{"one", "two"}
		body, err := client.getRequestBody("application/xml", requestBody)

		if err != nil {
			t.Error("no error expected when marshalling slice as XML")
		}
		if string(body) != `<string>one</string><string>two</string>` {
			t.Error("invalid XML body obtained")
		}
	})

	t.Run("BodyWithDefault", func(t *testing.T) {

		requestBody := []string{"one", "two"}
		body, err := client.getRequestBody("application/something-else", requestBody)

		if err != nil {
			t.Error("no error expected when marshalling slice as JSON")
		}
		if string(body) != `["one","two"]` {
			t.Error("invalid Default body obtained")
		}
	})
}

func Test_httpClient_getRequestHeaders(t *testing.T) {

	// Initialization
	client := httpClient{}
	commonHeaders := make(http.Header)
	commonHeaders.Set("Content-Type", "application/json")
	commonHeaders.Set("User-Agent", "cool-http-client")
	client.builder = &clientBuilder{
		headers: commonHeaders,
	}

	// Execution
	requestHeaders := make(http.Header)
	requestHeaders.Set("X-Request-Id", "ABC-123")

	finalHeaders := client.getRequestHeaders(requestHeaders)

	// Validation
	if len(finalHeaders) != 3 {
		t.Error("expect 3 headers got", len(finalHeaders))
	}

	if finalHeaders.Get("X-Request-Id") != "ABC-123" {
		t.Error("expect ABC-123 got", finalHeaders.Get("X-Request-Id"))
	}

	if finalHeaders.Get("Content-Type") != "application/json" {
		t.Error("expect application/json got", finalHeaders.Get("Content-Type"))
	}

	if finalHeaders.Get("User-Agent") != "cool-http-client" {
		t.Error("expect cool-http-client got", finalHeaders.Get("User-Agent"))
	}
}
