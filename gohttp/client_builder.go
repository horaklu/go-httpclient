package gohttp

import (
	"net/http"
	"time"
)

type clientBuilder struct {
	headers            http.Header
	connectionTimeout  time.Duration
	responseTimeout    time.Duration
	maxIdleConnections int
	disableTimeouts    bool
	client             *http.Client
	userAgent          string
}

type ClientBuilder interface {
	SetHeaders(headers http.Header) ClientBuilder
	SetConnectionTimeout(timeout time.Duration) ClientBuilder
	SetResponseTimeout(timeout time.Duration) ClientBuilder
	SetMaxIdleConnections(connections int) ClientBuilder
	DisableTimeouts(disableTimeouts bool) ClientBuilder
	SetHttpClient(c *http.Client) ClientBuilder
	SetUserAgent(userAgent string) ClientBuilder

	Build() Client
}

func NewBuilder() ClientBuilder {
	return &clientBuilder{}
}

func (cb *clientBuilder) Build() Client {
	return &httpClient{
		builder: cb,
	}
}

func (cb *clientBuilder) SetHeaders(headers http.Header) ClientBuilder {
	cb.headers = headers
	return cb
}
func (cb *clientBuilder) SetConnectionTimeout(timeout time.Duration) ClientBuilder {
	cb.connectionTimeout = timeout
	return cb
}
func (cb *clientBuilder) SetResponseTimeout(timeout time.Duration) ClientBuilder {
	cb.responseTimeout = timeout
	return cb
}
func (cb *clientBuilder) SetMaxIdleConnections(connections int) ClientBuilder {
	cb.maxIdleConnections = connections
	return cb
}
func (cb *clientBuilder) DisableTimeouts(disableTimeouts bool) ClientBuilder {
	cb.disableTimeouts = disableTimeouts
	return cb
}

func (cb *clientBuilder) SetHttpClient(client *http.Client) ClientBuilder {
	cb.client = client
	return cb
}
func (cb *clientBuilder) SetUserAgent(userAgent string) ClientBuilder {
	cb.userAgent = userAgent
	return cb
}
