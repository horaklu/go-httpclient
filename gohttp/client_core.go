package gohttp

import (
	"bitbucket.org/horaklu/go-httpclient/core"
	"bitbucket.org/horaklu/go-httpclient/gohttp_mock"
	"bitbucket.org/horaklu/go-httpclient/gomime"
	"bytes"
	"encoding/json"
	"encoding/xml"
	"errors"
	"io"
	"net"
	"net/http"
	"strings"
	"time"
)

const (
	defaultMaxIdleConnections = 5
	defaultResponseTimeout    = 4 * time.Second
	defaultConnectionTimeout  = 3 * time.Second
)

func (hc *httpClient) getRequestBody(contentType string, body interface{}) ([]byte, error) {
	if body == nil {
		return nil, nil
	}

	switch strings.ToLower(contentType) {
	case gomime.ContentTypeJson:
		return json.Marshal(body)
	case gomime.ContentTypeXml:
		return xml.Marshal(body)
	default:
		return json.Marshal(body)
	}

}

func (hc *httpClient) do(method, url string, headers http.Header, body interface{}) (*core.Response, error) {

	fullHeaders := hc.getRequestHeaders(headers)

	requestBody, err := hc.getRequestBody(fullHeaders.Get("Content-Type"), body)
	if err != nil {
		return nil, err
	}

	// create a request
	req, err := http.NewRequest(method, url, bytes.NewBuffer(requestBody))
	if err != nil {
		return nil, errors.New("unable to create a new  request:")
	}

	// add all headers to request
	req.Header = fullHeaders

	response, err := hc.getHttpClient().Do(req)
	if err != nil {
		return nil, err
	}
	defer response.Body.Close()
	responseBody, err := io.ReadAll(response.Body)
	if err != nil {
		return nil, err
	}

	return &core.Response{
		Status:     response.Status,
		StatusCode: response.StatusCode,
		Headers:    response.Header,
		Body:       responseBody,
	}, nil
}

func (hc *httpClient) getHttpClient() core.HttpClient {

	if gohttp_mock.MockupServer.IsEnabled() {
		return gohttp_mock.MockupServer.GetMockedClient()
	}

	// I need to create only one client for all concurrent calls
	hc.clientOnce.Do(func() {
		if hc.builder.client != nil {
			hc.client = hc.builder.client
			return
		}
		hc.client = &http.Client{
			Timeout: hc.getConnectionTimeout() + hc.getResponseTimeout(),
			Transport: &http.Transport{
				MaxIdleConnsPerHost:   hc.getMaxIdleConnections(),
				ResponseHeaderTimeout: hc.getResponseTimeout(),
				DialContext: (&net.Dialer{
					Timeout: hc.getConnectionTimeout(),
				}).DialContext,
			},
		}
	})

	return hc.client
}

func (hc *httpClient) getMaxIdleConnections() int {
	if hc.builder.maxIdleConnections > 0 {
		return hc.builder.maxIdleConnections
	}
	return defaultMaxIdleConnections
}

func (hc *httpClient) getResponseTimeout() time.Duration {
	if hc.builder.disableTimeouts {
		return 0
	}
	if hc.builder.responseTimeout > 0 {
		return hc.builder.responseTimeout
	}
	return defaultResponseTimeout
}

func (hc *httpClient) getConnectionTimeout() time.Duration {
	if hc.builder.disableTimeouts {
		return 0
	}
	if hc.builder.connectionTimeout > 0 {
		return hc.builder.connectionTimeout
	}
	return defaultConnectionTimeout
}
