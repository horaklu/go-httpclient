package examples

import (
	"bitbucket.org/horaklu/go-httpclient/gohttp"
	"bitbucket.org/horaklu/go-httpclient/gomime"
	"net/http"
	"time"
)

var httpClient = getHttpClient()

func getHttpClient() gohttp.Client {
	headers := make(http.Header)
	headers.Set(gomime.HeaderContentType, gomime.ContentTypeJson)

	client := gohttp.NewBuilder().
		SetHeaders(headers).
		SetConnectionTimeout(10 * time.Second).
		SetResponseTimeout(3 * time.Second).
		Build()

	return client
}
