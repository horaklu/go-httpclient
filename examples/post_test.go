package examples

import (
	"bitbucket.org/horaklu/go-httpclient/gohttp_mock"
	"errors"
	"fmt"
	"net/http"
	"testing"
)

func TestCreateRepository(t *testing.T) {

	t.Run("timeout from github", func(t *testing.T) {
		gohttp_mock.MockupServer.DeleteMocks()
		gohttp_mock.MockupServer.AddMock(gohttp_mock.Mock{
			Method:      http.MethodPost,
			Url:         "https://api.github.com/user/repos",
			RequestBody: `{"name":"test-repo","description":"","private":true}`,
			Error:       errors.New("timeout from github"),
		})

		repository := Repository{
			Name:    "test-repo",
			Private: true,
		}
		repo, err := CreateRepository(repository)

		if repo != nil {
			t.Error("no repo expected - because of timeout")
		}
		if err == nil {
			t.Error("an error was expected - because of timeout")
		}
		if err.Error() != "timeout from github" {
			t.Error("invalid error message when timeout error happens")
		}
	})

	t.Run("no error", func(t *testing.T) {
		gohttp_mock.MockupServer.DeleteMocks()
		gohttp_mock.MockupServer.AddMock(gohttp_mock.Mock{
			Method:             http.MethodPost,
			Url:                "https://api.github.com/user/repos",
			RequestBody:        `{"name":"test-repo","description":"","private":true}`,
			ResponseStatusCode: http.StatusCreated,
			ResponseBody:       `{"id":123,"name":"test-repo"}`,
		})

		repository := Repository{
			Name:    "test-repo",
			Private: true,
		}
		repo, err := CreateRepository(repository)

		if err != nil {
			t.Error("no error expected - everything should be ok")
		}
		if repo == nil {
			t.Error("a valid repo was expected - everything should be ok")
		}
		if repo.Name != repository.Name {
			t.Error("invalid repo name obtained from github")
		}
	})

	t.Run("received giberish from github", func(t *testing.T) {
		gohttp_mock.MockupServer.DeleteMocks()
		gohttp_mock.MockupServer.AddMock(gohttp_mock.Mock{
			Method:             http.MethodPost,
			Url:                "https://api.github.com/user/repos",
			RequestBody:        `{"name":"test-repo","description":"","private":true}`,
			ResponseStatusCode: http.StatusCreated,
			ResponseBody:       `{"this","cannot","be","unmarshalled"}`,
		})

		repository := Repository{
			Name:    "test-repo",
			Private: true,
		}
		repo, err := CreateRepository(repository)

		if repo != nil {
			t.Error("no repo expected - cannot unmarshal response")
		}
		if err == nil {
			t.Error("an error was expected - cannot unmarshal response")
		}
		fmt.Println(err)
		if err.Error() != "error unmarshal response" {
			t.Error(fmt.Sprintf("invalid error message: expected 'error unmarshal response', got: %s", err.Error()))
		}
	})

	t.Run("bad status code received correct error code", func(t *testing.T) {
		gohttp_mock.MockupServer.DeleteMocks()
		gohttp_mock.MockupServer.AddMock(gohttp_mock.Mock{
			Method:             http.MethodPost,
			Url:                "https://api.github.com/user/repos",
			RequestBody:        `{"name":"test-repo","description":"","private":true}`,
			ResponseStatusCode: http.StatusNotFound,
			ResponseBody:       `{"message":"Not Found","documentation_url":"https://docs.github.com/rest"}`,
		})

		repository := Repository{
			Name:    "test-repo",
			Private: true,
		}

		repo, err := CreateRepository(repository)

		if repo != nil {
			t.Error("no repo expected - because of wrong status code")
		}
		if err == nil {
			t.Error("an error was expected - because of status code")
		}
		if err.Error() != "Not Found" {
			t.Error(fmt.Sprintf("expected error 'Not Found' got: %s", err.Error()))
		}
	})

	t.Run("bad status code received invalid error code", func(t *testing.T) {
		gohttp_mock.MockupServer.DeleteMocks()
		gohttp_mock.MockupServer.AddMock(gohttp_mock.Mock{
			Method:             http.MethodPost,
			Url:                "https://api.github.com/user/repos",
			RequestBody:        `{"name":"test-repo","description":"","private":true}`,
			ResponseStatusCode: http.StatusNotFound,
			ResponseBody:       `{"unmarhsable","to","json"}`,
		})

		repository := Repository{
			Name:    "test-repo",
			Private: true,
		}

		repo, err := CreateRepository(repository)
		fmt.Println(err)

		if repo != nil {
			t.Error("no repo expected - because of wrong status code")
		}
		if err == nil {
			t.Error("an error was expected - because of status code")
		}
		if err.Error() != "error processing github error response when creating a new repo" {
			t.Error(fmt.Sprintf("invalid error message\nexpected error: 'error processing github error response when creating a new repo'\ngot: %s", err.Error()))
		}
	})
}
