package examples

import (
	"errors"
	"net/http"
)

type Repository struct {
	Name        string `json:"name"`
	Description string `json:"description"`
	Private     bool   `json:"private"`
}

type GithubError struct {
	StatusCode       int    `json:"-"`
	Message          string `json:"message"`
	DocumentationUrl string `json:"documentation_url"`
}

func CreateRepository(request Repository) (*Repository, error) {
	response, err := httpClient.Post("https://api.github.com/user/repos", request)
	if err != nil {
		return nil, err
	}

	if response.StatusCode != http.StatusCreated {
		var githubErr GithubError
		err = response.UnmarshalJSON(&githubErr)
		if err != nil {
			return nil, errors.New("error processing github error response when creating a new repo")
		}
		return nil, errors.New(githubErr.Message)
	}

	var result Repository
	if err = response.UnmarshalJSON(&result); err != nil {
		return nil, errors.New("error unmarshal response")
	}

	return &result, nil
}
