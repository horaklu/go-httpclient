package gohttp_mock

import (
	"bitbucket.org/horaklu/go-httpclient/core"
	"crypto/md5"
	"encoding/hex"
	"fmt"
	"strings"
	"sync"
)

type mockServer struct {
	enabled     bool
	serverMutex sync.Mutex

	httpClient core.HttpClient

	mocks map[string]*Mock
}

var MockupServer = mockServer{
	mocks:      make(map[string]*Mock),
	httpClient: &httpClientMock{},
}

func (ms *mockServer) Start() {
	ms.serverMutex.Lock()
	defer ms.serverMutex.Unlock()
	ms.enabled = true
}

func (ms *mockServer) IsEnabled() bool {
	return ms.enabled
}

func (ms *mockServer) GetMockedClient() core.HttpClient {
	return ms.httpClient
}

func (ms *mockServer) Stop() {
	ms.serverMutex.Lock()
	defer ms.serverMutex.Unlock()
	MockupServer.enabled = false
}

func (ms *mockServer) AddMock(mock Mock) {
	ms.serverMutex.Lock()
	defer ms.serverMutex.Unlock()
	key := MockupServer.getMockKey(mock.Method, mock.Url, mock.RequestBody)
	MockupServer.mocks[key] = &mock
}

func (ms *mockServer) DeleteMocks() {
	ms.serverMutex.Lock()
	defer ms.serverMutex.Unlock()
	MockupServer.mocks = make(map[string]*Mock)
}

func (ms *mockServer) getMockKey(method, url, body string) string {
	hasher := md5.New()
	hasher.Write([]byte(fmt.Sprintf("%s%s%s", method, url, ms.cleanBody(body))))
	key := hex.EncodeToString(hasher.Sum(nil))
	return key
}

func (ms *mockServer) cleanBody(body string) string {
	body = strings.TrimSpace(body)
	if body == "" {
		return ""
	}
	body = strings.ReplaceAll(body, "\t", "")
	body = strings.ReplaceAll(body, "\n", "")
	return body
}
