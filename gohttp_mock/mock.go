package gohttp_mock

import (
	"bitbucket.org/horaklu/go-httpclient/core"
	"fmt"
	"net/http"
)

// Mock configure HTTP mocks based on Method, Url and RequestBody
type Mock struct {
	Method      string
	Url         string
	RequestBody string

	ResponseBody       string
	ResponseStatusCode int
	Error              error
}

// GetResponse returns a Response object based on mock configuration
func (m *Mock) GetResponse() (*core.Response, error) {
	if m.Error != nil {
		return nil, m.Error
	}

	return &core.Response{
		Status:     fmt.Sprintf("%d %s", m.ResponseStatusCode, http.StatusText(m.ResponseStatusCode)),
		StatusCode: m.ResponseStatusCode,
		Body:       []byte(m.ResponseBody),
	}, nil
}
